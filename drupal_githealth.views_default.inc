<?php
/**
 * @file
 * Provides default information to views.
 */

/**
 * Implements hook_views_default_views().
 */
function drupal_githealth_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'drupal_githealth_result';
  $view->description = '';
  $view->tag = '';
  $view->base_table = 'field_revision_conduit_result_drupal_summary';
  $view->human_name = 'Githealth result';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'conduit-drupal';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = 'conduit_result_drupal_summary_value_raw';
  $handler->display->display_options['style_options']['row_class'] = 'drupal-githealth-[conduit_result_drupal_type_value_raw]';
  $handler->display->display_options['style_options']['columns'] = array(
    'conduit_result_drupal_summary_value_raw' => 'conduit_result_drupal_summary_value_raw',
    'conduit_result_drupal_message_value_raw' => 'conduit_result_drupal_message_value_raw',
    'conduit_result_drupal_group_value_raw' => 'conduit_result_drupal_group_value_raw',
    'conduit_result_drupal_file_value_raw' => 'conduit_result_drupal_file_value_raw',
    'conduit_result_drupal_line_value_raw' => 'conduit_result_drupal_line_value_raw',
    'conduit_result_drupal_function_value_raw' => 'conduit_result_drupal_function_value_raw',
    'conduit_result_drupal_type_value_raw' => 'conduit_result_drupal_type_value_raw',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'conduit_result_drupal_summary_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_message_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_group_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_file_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_line_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_function_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'conduit_result_drupal_type_value_raw' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No results';
  $handler->display->display_options['empty']['area']['content'] = 'No results to display.';
  /* Field: Revision tables: Summary => Value */
  $handler->display->display_options['fields']['conduit_result_drupal_summary_value_raw']['id'] = 'conduit_result_drupal_summary_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_summary_value_raw']['table'] = 'field_revision_conduit_result_drupal_summary';
  $handler->display->display_options['fields']['conduit_result_drupal_summary_value_raw']['field'] = 'conduit_result_drupal_summary_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_summary_value_raw']['label'] = '';
  $handler->display->display_options['fields']['conduit_result_drupal_summary_value_raw']['exclude'] = TRUE;
  /* Field: Revision tables: Type => Value */
  $handler->display->display_options['fields']['conduit_result_drupal_type_value_raw']['id'] = 'conduit_result_drupal_type_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_type_value_raw']['table'] = 'field_revision_conduit_result_drupal_type';
  $handler->display->display_options['fields']['conduit_result_drupal_type_value_raw']['field'] = 'conduit_result_drupal_type_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_type_value_raw']['label'] = 'Status';
  /* Field: Revision tables: Message => Value */
  $handler->display->display_options['fields']['conduit_result_drupal_message_value_raw']['id'] = 'conduit_result_drupal_message_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_message_value_raw']['table'] = 'field_revision_conduit_result_drupal_message';
  $handler->display->display_options['fields']['conduit_result_drupal_message_value_raw']['field'] = 'conduit_result_drupal_message_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_message_value_raw']['label'] = 'Message';
  /* Field: Revision tables: File => Value */
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['id'] = 'conduit_result_drupal_file_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['table'] = 'field_revision_conduit_result_drupal_file';
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['field'] = 'conduit_result_drupal_file_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['label'] = 'Item';
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['alter']['max_length'] = '50';
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['conduit_result_drupal_file_value_raw']['alter']['trim'] = TRUE;
  /* Field: Revision tables: Function => Value */
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['id'] = 'conduit_result_drupal_function_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['table'] = 'field_revision_conduit_result_drupal_function';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['field'] = 'conduit_result_drupal_function_value_raw';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['label'] = 'Drupal.org Reference';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['alter']['path'] = 'http://drupal.org/node/[conduit_result_drupal_function_value_raw]';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['alter']['max_length'] = '50';
  $handler->display->display_options['fields']['conduit_result_drupal_function_value_raw']['alter']['word_boundary'] = FALSE;
  /* Sort criterion: Revision tables: conduit_result_drupal_message => Delta */
  $handler->display->display_options['sorts']['delta_raw']['id'] = 'delta_raw';
  $handler->display->display_options['sorts']['delta_raw']['table'] = 'field_revision_conduit_result_drupal_message';
  $handler->display->display_options['sorts']['delta_raw']['field'] = 'delta_raw';
  /* Contextual filter: Revision tables: conduit_result_drupal_summary => Entity id */
  $handler->display->display_options['arguments']['entity_id']['id'] = 'entity_id';
  $handler->display->display_options['arguments']['entity_id']['table'] = 'field_revision_conduit_result_drupal_summary';
  $handler->display->display_options['arguments']['entity_id']['field'] = 'entity_id';
  $handler->display->display_options['arguments']['entity_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['entity_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['entity_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['entity_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['entity_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['entity_id']['validate_options']['types'] = array(
    'conduit_job_drupal_githealth' => 'conduit_job_drupal_githealth',
  );
  /* Contextual filter: Revision tables: conduit_result_drupal_summary => Revision id */
  $handler->display->display_options['arguments']['revision_id_raw']['id'] = 'revision_id_raw';
  $handler->display->display_options['arguments']['revision_id_raw']['table'] = 'field_revision_conduit_result_drupal_summary';
  $handler->display->display_options['arguments']['revision_id_raw']['field'] = 'revision_id_raw';
  $handler->display->display_options['arguments']['revision_id_raw']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['revision_id_raw']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['revision_id_raw']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['revision_id_raw']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['revision_id_raw']['validate']['type'] = 'numeric';
  /* Filter criterion: Revision tables: Item => Value */
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['id'] = 'conduit_result_drupal_item_value_raw';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['table'] = 'field_revision_conduit_result_drupal_item';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['field'] = 'conduit_result_drupal_item_value_raw';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['operator'] = 'contains';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['exposed'] = TRUE;
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['expose']['operator_id'] = 'conduit_result_drupal_item_value_raw_op';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['expose']['label'] = 'Test';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['expose']['operator'] = 'conduit_result_drupal_item_value_raw_op';
  $handler->display->display_options['filters']['conduit_result_drupal_item_value_raw']['expose']['identifier'] = 'test';

  $views[$view->name] = $view;

  return $views;
}

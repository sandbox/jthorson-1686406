<?php
/**
 * @file
 * Provide Drupal githealth worker plugin.
 */

// TODO: Throw errors if the plugin receives a bad return code



/**
 * GitHealth Severity levels
 */
define('GITHEALTH_CRITICAL', 0);
define('GITHEALTH_MAJOR', 1);
define('GITHEALTH_MINOR', 2);

$plugin = array(
  'title' => t('Drupal Githealth'),
  'description' => t('Provide Drupal repository health check worker plugin.'),
  'perform' => 'githealth_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 *
 * @return
 *   An array containing a boolean for pass/fail and the result. The result is
 *   a multi-dimentional, associative array of githealth results keyed by
 *   the healthcheck category and containing an array of messages with the keys
 *   type, message, function, group, and file; and a sum of the number of
 *   messages of each type.
 *
 *   @code
 *     $result = array(
 *       '#total' => array(1, 0, 0),
 *       'Branches' => array(
 *         '#total' => array(1, 0, 0),
 *         array(
 *           'type' => 'INFO', // CRITICAL|MAJOR|MINOR|INFO
 *           'message' => 'A text string describing the issue',
 *           'function' => '[see #1234]', // Reference link to drupal.org node
 *           'group' => 'Some Group',
 *           'file' => 'file.php',
 *         ),
 *       ),
 *     );
 *   @endcode
 *
 */
function githealth_perform(array $properties) {

  // TODO: Add 'branch' property to restrict plugin to a specific branch

  // Prepare results arrays
  $map = array_flip(array('critical', 'major', 'minor'));
  $results = array(
    '#total' => array(0, 0, 0),
    'Branches' => array(
      '#total' => array(0, 0, 0),
    ),
    'Tags' => array(
      '#total' => array(0, 0, 0),
    ),
    'Files' => array(
      '#total' => array(0, 0, 0),
    ),
  );

  // Perform the job
  worker_log('Beginning Git Repository health check.');
  worker_chdir();

  // Use the 'is_core' property to determine whether is core or not, as core
  // has a different tag/branch naming format
  $iscore = ($properties['project'] == 'drupal');
  // Retrieve the project shortname
  $shortname = $properties['project'];

  // Retrieve all branches
  $branches = githealth_branches();
  if ($branches === FALSE) {
  	// Error encountered retrieving branches
    worker_log(t('ERROR: ') . t('Error encountered while retrieving list of repository branches.'));
  	return array(FALSE, t('Error encountered while retrieving list of repository branches.'));
  }

  // Retrieve all tags
  $tags = githealth_tags();
  if ($tags === FALSE) {
    // Error encountered retrieving tags
    worker_log(t('ERROR: ') . t('Error encountered while retrieving list of repository tags.'));
    return array(FALSE, t('Error encountered while retrieving list of repository tags.'));
  }

  // Retrieve all files
  $files = githealth_files(worker_file_list($properties));

  // Validate branch names
  githealth_check_branchnames($results, $branches, $iscore);

  // Validate tag names
  githealth_check_tagnames($results, $tags, $iscore);

  // Check for branch/tag conflicts
  githealth_check_conflicts($results, $branches, $tags);

  // Validate files and directories
  githealth_check_files($results, $files, $shortname);

  // Validate file contents
  githealth_check_files_content($results, $files, $shortname);

  // PHP Syntax check
  $syntax = worker_syntax($properties);
  if ($syntax === FALSE || !empty($syntax)) {
    $message .= t('Please check the detailed review log for a list of files which failed the PHP syntax check. ');
    worker_log(t('CRITICAL: ') . $message);
    //githealth_results($results, $message, 'branches', 'critical');
  }

  // TODO:  Sandbox checks?

  // TODO:  Project checks?

  // If there are no major or critical items, then consider the scan a pass.
  return array($results['#total'][0] + $results['#total'][1] == 0, $results);
}

/**
 * Return a list of branches found in the repository
 */
function githealth_branches() {
  worker_log('Attempting to retrieve branch list.');
  worker_chdir();
  if ($output = worker_execute_output('git branch -a')) {
    $branches = array();
    foreach ($output as $branch) {
  	  // Detect remote HEAD branch, and set within a special key
  	  if (preg_match('|^  remotes/origin/HEAD -> origin/(.*)$|', $branch, $matches)) {
  	    $branches['HEAD'] = $matches[1];
  	  }
  	  // Only add remote branches, to avoid duplicate listings
  	  elseif (preg_match('|^  remotes/origin/(.*)$|', $branch, $matches)) {
  	    $branches[] = $matches[1];
  	  }
  	}
  	return $branches;
  }
  else {
    // Unable to retrieve branches
    return FALSE;
  }
}

/**
 * Return a list of tags found in the repository
 */
function githealth_tags() {
  worker_log(t('Attempting to retrieve tag list.'));
  worker_chdir();
  if ($output = worker_execute_output('git tag -l')) {
    if (!is_array($output)) {
      // Command returned a non-zero exit code.
      return FALSE;
    }
    $tags = array();
    foreach ($output as $tag) {
      $tags[] = $tag;
    }
    return $tags;
  }
  else {
    // No tags found
    return array();
  }
}

/**
 * Return a sorted list of files found in the repository
 *
 * @param $filelist
 *   A list of files as generated by worker_file_list().
 *
 * @return
 *   Multi-dimensional, sorted array of files
 *
 * @code
 *   $files = array(
 *     'all' => array(), 			// All files
 *     'filenames' => array(), 		// All filenames (no paths)
 *     'directories' => array(), 	// List of unique directories
 *     'php' => array(),			// All php files
 *     'code' => array(),			// All php / js files
 *     'text' => array(), 			// All text-based files
 *     'nontpl' => array(),			// All non-tpl php files
 *     'checkfunctions' => array(),	// All php excluding api.php and drush.inc
 *     'info' => array(),			// All .info files
 *     'other' => array(),			// All other files
 *   );
 * @endcode
 */
function githealth_files($filelist) {
  worker_chdir();
  $files['all'] = array();
  $directories = array();
  foreach ($filelist as $file) {
    // Determine file path
    $path_separator = strrpos($file, '/');
    $path = substr($file, 0, $path_separator);
    // Determine file name
    $filename = substr($file, $path_separator + 1, strlen($file) - ($path_separator + 1));
    // Determine file extension
    $components = explode('.', $file);
    $extension = array_pop($components);

    // Identify 'special case' files
    $subtype = array_pop($components);
    $is_tpl = ($extension == 'php' && $subtype == 'tpl');
    $is_api = ($extension == 'php' && $subtype == 'api');
    $is_drushinc = ($extension == 'inc' && $subtype == 'drush');

    // Add file to 'all' file listings
    $files['all'][] = $file;
    $files['filenames'][] = $filename;
    $directories[] = $path;

    switch ($extension) {
      case 'module':
      case 'php':
      case 'inc':
      case 'install':
      case 'test':
        $files['php'][] = $file;
        $files['code'][] = $file;
        $files['text'][] = $file;
        if (!$is_tpl) {
          $files['nontpl'][] = $file;
        }
        if (!$is_api && !$is_drushinc) {
          $files['checkfunctions'][] = $file;
        }
        break;
      case 'profile':
        $files['php'][] = $file;
        $files['nontpl'][] = $file;
        $files['checkfunctions'][] = $file;
        break;
      case 'js':
        $files['code'][] = $file;
        $files['text'][] = $file;
        break;
      case 'info':
        $files['info'][] = $file;
      case 'css':
      case 'txt':
        $files['text'][] = $file;
        break;
      default:
        $files['other'][] = $file;
    }
  }
  // Remove duplicate paths
  $files['directories'] = array_unique($directories);

  return $files;
}

/**
 * Evaluates branch names, looking for naming violations and other issues.
 *
 * @param $results
 *   Array for results storage, passed in by reference
 * @param $branches
 *   Array of branch names found in the repository
 * @param boolean $iscore
 *   Flag indicating whether the project is 'Drupal Core'
 */
function githealth_check_branchnames(&$results, $branches, $iscore) {
  // Validate repository branch names
  if (count($branches) > 0) {
    $head = $branches['HEAD'];
    unset($branches['HEAD']);
    worker_log(t('Evaluating !count branch names.', array('!count' => count($branches))));

    // Create 'info' result entry listing available branches.
    $message = t('!count branches found: ', array('!count' => count($branches)));
    $message .= implode(', ', $branches);
    $reference = '';
    $results['Branches'][] = array(
      'type' => 'INFO',
      'message' => $message,
      'function' => $reference,
      'file' => '',
    );

    // Select a regex to evaluate repository branch names against
    $core_branch_regex = "/^((3|4).)?\d\.x$/";
    $contrib_branch_regex = "/^\d\.x-\d{1,2}\.x$/";
    $regex = ($iscore) ? $core_branch_regex : $contrib_branch_regex;

    // Determine default branch.
    if (isset($head)) {
      // Ensure default is set to a major version branch
      if (!(preg_match($regex, $head))) {
        $results['#total'][GITHEALTH_CRITICAL]++;
        $results['Branches']['#total'][GITHEALTH_CRITICAL]++;
        $message = t('It appears you are working in the "@branch" branch in git. ');
        $message .= t('Please update your default branch to point at a major version branch.');
        $reference = '1127732';
        $results['Branches'][] = array(
          'type' => 'CRITICAL',
          'message' => $message,
          'function' => $reference,
          'file' => $head,
        );
      }
    }

    // Check for presence of a 'master' branch
    if (in_array('master', $branches)) {
      $results['#total'][GITHEALTH_MAJOR]++;
      $results['Branches']['#total'][GITHEALTH_MAJOR]++;
      $message = t('It appears that your repository still contains a master branch. ');
      $message .= t('After you have set a new default branch, the master branch should be removed. ');
      $reference = '1127732';
      $results['Branches'][] = array(
        'type' => 'MAJOR',
        'message' => $message,
        'function' => $reference,
        'file' => 'master',
      );
    }

    // Evaluate branch names against Drupal's release naming conventions, as
    // documented at http://drupal.org/node/1015226.
    foreach ($branches as $branch) {
      if (!(preg_match($regex, $branch)) && ($branch != 'master')) {
        // Branch name does not match Drupal's naming patterns.
        $results['#total'][GITHEALTH_MINOR]++;
        $results['Branches']['#total'][GITHEALTH_MINOR]++;
        $message = t('Non-standard branch name. ');
        $message .= t('These are allowed within a repository, but can not be used to generate a release on Drupal.org. ');
        $reference = '1015226';
        $results['Branches'][] = array(
          'type' => 'MINOR',
          'message' => $message,
          'function' => $reference,
          'file' => $branch,
        );
        // Complain if contrib projects are using the core naming pattern.
        if (!$iscore && preg_match($core_branch_regex, $branch)) {
          $results['#total'][GITHEALTH_MAJOR]++;
          $results['Branches']['#total'][GITHEALTH_MAJOR]++;
          $message = t('This branch naming format is reserved for Drupal core. ');
          $message .= t('Contrib module branches should follow a "MAJOR.x-MINOR.x" pattern. ');
          $reference = '1015226';
          $results['Branches'][] = array(
            'type' => 'MAJOR',
            'message' => $message,
            'function' => $reference,
            'file' => $branch,
          );
        }
      }
      else {
        $has_release_branch = TRUE;
      }
    }
    if (!$has_release_branch) {
      $results['#total'][GITHEALTH_CRITICAL]++;
      $results['Branches']['#total'][GITHEALTH_CRITICAL]++;
      $message = t('Repository does not contain any branch names matching Drupal\'s release naming conventions. ');
      $reference = '1015226';
      $results['Branches'][] = array(
        'type' => 'CRITICAL',
        'message' => $message,
        'function' => $reference,
        'file' => $head,
      );
    }
  }
  else {
    // No branches found.
    // I don't believe this is a valid outcome, but it will catch a scenario
    // where githealth_branches returns an empty array due to some logic flaw
    // or change in the git branch command output.
    $results['#total'][GITHEALTH_CRITICAL]++;
    $results['Branches']['#total'][GITHEALTH_CRITICAL]++;
    $message = t('No branches found in repository. ');
    $reference = '';
    $results['Branches'][] = array(
      'type' => 'CRITICAL',
      'message' => $message,
      'function' => $reference,
      'file' => 'Repository',
    );
  }
}

/**
 * Evaluates tag names, looking for naming violations and other issues.
 *
 * @param $results
 *   Array for results storage, passed in by reference
 * @param $tags
 *   Array of tag names found in the repository
 * @param boolean $iscore
 *   Flag indicating whether the project is 'Drupal Core'
 */
function githealth_check_tagnames(&$results, $tags, $iscore) {
  // Validate repository tag names
  if (count($tags) > 0) {
    worker_log(t('Evaluating !count tag names.', array('!count' => count($tags))));

    // Create 'info' result entry listing available tags.
    $message = t('!count tags found: ', array('!count' => count($tags)));
    $message .= implode(', ', $tags);
    $reference = '';
    $results['Tags'][] = array(
      'type' => 'INFO',
      'message' => $message,
      'function' => $reference,
      'file' => '',
    );

    // Select a regex to evaluate repository tag names against
    $core_tag_regex = "/^((3|4).)?\d\.\d{1,2}(-(unstable|alpha|beta|rc|security)(-)?\d{1,2})?$/";
    $contrib_tag_regex = "/^\d\.x-\d{1,2}\.\d{1,2}(-(unstable|alpha|beta|rc)\d)?$/";
    $regex = ($iscore) ? $core_tag_regex : $contrib_tag_regex;

    // Loop through available branches
    foreach ($tags as $tag) {
      // Evaluate tag names against Drupal's release naming conventions, as
      // documented at http://drupal.org/node/1015226.
      if (!(preg_match($regex, $tag))) {
        // Tag name does not match Drupal's naming patterns.
        $results['#total'][GITHEALTH_MINOR]++;
        $results['Tags']['#total'][GITHEALTH_MINOR]++;
        $message = t('Non-standard tag name. ');
        $message .= t('These are allowed within a repository, but can not be used to generate a release on Drupal.org. ');
        $reference = '1015226';
        $results['Tags'][] = array(
          'type' => 'MINOR',
          'message' => $message,
          'function' => $reference,
          'file' => $tag,
        );
        // Complain if contrib projects are using the core naming pattern.
        if (!$iscore && preg_match($core_tag_regex, $tag)) {
          $results['#total'][GITHEALTH_MAJOR]++;
          $results['Tags']['#total'][GITHEALTH_MAJOR]++;
          $message = t('This tag naming format is reserved for Drupal core. ');
          $message .= t('Contrib module branches should follow a "MAJOR.x-MINOR.x" pattern. ');
          $reference = '1015226';
          $results['Tags'][] = array(
            'type' => 'MAJOR',
            'message' => $message,
            'function' => $reference,
            'file' => $tag,
          );
        }
      }
      else {
        $has_release_tag = TRUE;
      }
    }
    // Ensure at least one tag matches the release naming patterns
    if (!$has_release_tag) {
      $results['#total'][GITHEALTH_MAJOR]++;
      $results['Tags']['#total'][GITHEALTH_MAJOR]++;
      $message = t('Repository does not contain any tag names matching Drupal\'s release naming conventions. ');
      $message .= t('A valid tag will be required before a release can be generated for this project on Drupal.org.');
      $reference = '1015226';
      $results['Tags'][] = array(
        'type' => 'CRITICAL',
        'message' => $message,
        'function' => $reference,
        'file' => 'Repository',
      );
    }
  }
  else {
    // No tags found in repository. This is the same message as the previous
    // error, but references the version control page explaining how to create
    // a tag instead of the naming conventions page (which was linked since the
    // maintainer has already created at least one tag to trip the previous
    // check).
    if (!$has_release_tag) {
      $results['#total'][GITHEALTH_MAJOR]++;
      $results['Tags']['#total'][GITHEALTH_MAJOR]++;
      $message = t('Repository does not contain any tag names matching Drupal\'s release naming conventions. ');
      $message .= t('A valid tag will be required before a release can be generated for this project on Drupal.org.');
      $reference = '1015226';
      $results['Tags'][] = array(
        'type' => 'CRITICAL',
        'message' => $message,
        'function' => $reference,
        'file' => 'Repository',
      );
    }
  }
}

/**
 * Looks for branch/tag naming conflicts.
 *
 * @param $results
 *   Array for results storage, passed in by reference
 * @param $branches
 *   Array of branch names found in the repository
 * @param $tags
 *   Array of tag names found in the repository
 */
function githealth_check_conflicts(&$results, $branches, $tags) {
  worker_log(t('Checking for branch/tag name conflicts.'));
  $conflicts = array_intersect($branches, $tags);
  foreach ($conflicts as $branch) {
    $results['#total'][GITHEALTH_MAJOR]++;
    $results['Tags']['#total'][GITHEALTH_MAJOR]++;
    $message = t('You have a git tag named the same as a branch. ');
    $message .= t('It is recommended that you remove the tag to avoid confusion.');
    $reference = '1066342';
    $results['Tags'][] = array(
      'type' => 'MAJOR',
      'message' => $message,
      'function' => $reference,
      'file' => $branch,
    );
  }
}

/**
 * Evaluates file names, looking for naming violations and other issues.
 *
 * @param $results
 *   Array for results storage, passed in by reference
 * @param $files
 *   Array of files found in the repository, from githealth_files().
 * @param $shortname
 *   String indicating the project shortname
 */
function githealth_check_files(&$results, $files, $shortname) {
  worker_log(t('Evaluating !count files.', array('!count' => count($files['all']))));
  // Check for presence of a README.txt file
  if (!array_search('readme.txt', array_map('strtolower', $files['filenames']))) {
    $results['#total'][GITHEALTH_MAJOR]++;
    $results['Files']['#total'][GITHEALTH_MAJOR]++;
    $message = t('README.txt is missing. ');
    $message .= t('Please see the guidelines for in-project documentation. ');
    $reference = '447604';
    $results['Files'][] = array(
      'type' => 'MAJOR',
      'message' => $message,
      'function' => $reference,
      'file' => 'README.txt',
    );
  }

  // Check for presence of a LICENSE.txt file
  if (($shortname != 'drupal') && array_search('license.txt', array_map('strtolower', $files['filenames']))) {
    $results['#total'][GITHEALTH_MAJOR]++;
    $results['Files']['#total'][GITHEALTH_MAJOR]++;
    $message = t('Please remove the LICENSE.txt file. ');
    $message .= t('It will automatically be added by the drupal.org packaging scripts. ');
    $reference = '272651';
    $results['Files'][] = array(
      'type' => 'MAJOR',
      'message' => $message,
      'function' => $reference,
      'file' => 'LICENSE.txt',
    );
  }

  // Check for presence of common extraneous files
  foreach (array('.DS_Store', '.project', '.settings') as $filename) {
    if (in_array($filename, $files['filenames'])) {
      $results['#total'][GITHEALTH_MAJOR]++;
      $results['Files']['#total'][GITHEALTH_MAJOR]++;
      $message = t('Extraneous files in repository.');
      $reference = '';
      $results['Files'][] = array(
        'type' => 'MAJOR',
        'message' => $message,
        'function' => $reference,
        'file' => $filename,
      );
    }
  }

  // Check for presense of a translations folder
  if (in_array('./translations', $files['directories'])) {
    $results['#total'][GITHEALTH_MAJOR]++;
    $results['Files']['#total'][GITHEALTH_MAJOR]++;
    $message = t('Remove the translations folder from your repository. ');
    $message .= t('Translations are done on http://localize.drupal.org.');
    $reference = '296338';
    $results['Files'][] = array(
      'type' => 'MAJOR',
      'message' => $message,
      'function' => $reference,
      'file' => './translations',
    );
  }
}

/**
 * Evaluates file contents, looking for potential issues.
 *
 * @param $results
 *   Array for results storage, passed in by reference
 * @param $files
 *   Array of files found in the repository, from githealth_files().
 * @param $shortname
 *   String indicating the project shortname
 */
function githealth_check_files_content(&$results, $files, $shortname) {

  // Validate info files
  foreach (array('project', 'version', 'datestamp') as $needle) {
    foreach ($files['info'] as $file) {
      $commmand = 'grep -q -e "' . $needle . '[[:space:]]*=[[:space:]]*" ' . $file;
      $result = worker_execute_output($command);
      if ($result && $result != array()) {
        $results['#total'][GITHEALTH_MAJOR]++;
        $results['Files']['#total'][GITHEALTH_MAJOR]++;
        $message = t('Remove !needle from your .info file. ', array('!needle' => $needle));
        $message .= t('It will automatically be added by the drupal.org packaging scripts.');
        $reference = '542202';
        $results['Files'][] = array(
          'type' => 'MAJOR',
          'message' => $message,
          'function' => $reference,
          'file' => $file,
        );
      }
    }
  }

  // Check for PHP delimiters at the end of any files
  if ($shortname != 'drupal') {
    $command = 'grep -l "^\?>" ' . implode(' ', $files['nontpl']);
    $result = worker_execute_output($command);
    foreach ($result as $file) {
      $results['#total'][GITHEALTH_MINOR]++;
      $results['Files']['#total'][GITHEALTH_MINOR]++;
      $message = t('Use of the "?>" PHP delimiter at the end of files is discouraged.');
      $reference = '318#phptags';
      $results['Files'][] = array(
        'type' => 'MINOR',
        'message' => $message,
        'function' => $reference,
        'file' => $file,
      );
    }

    // Check for functions without the module prefix.
    foreach ($files['checkfunctions'] as $file) {
      $command = 'grep -E "^function [[:alnum:]_]+.*\(.*\) \{" ' . $file . ' | grep -v -E "^function (_?' . $shortname . '|theme|template|phptemplate)"';
      worker_chdir();
      $result = worker_execute_output($command);
      foreach ($result as $function) {
        $results['#total'][GITHEALTH_MAJOR]++;
        $results['Files']['#total'][GITHEALTH_MAJOR]++;
        $message = t('All functions should be prefixed with your module/theme name to avoid name clashes.');
        $reference = '318#naming';
        $results['Files'][] = array(
          'type' => 'MAJOR',
          'message' => $message,
          'function' => $reference,
          'file' => $function,
        );
      }
    }
  }

  // Check for bad line endings in files
  $command =  'file ' . implode(' ', $files['all']) . ' | grep "line terminators"';
  $result = worker_execute_output($command);
  foreach ($result as $line) {
    $params = explode(':', $line);
    $file = $params[0];
    $terminators = $params[1];
    $results['#total'][GITHEALTH_MAJOR]++;
    $results['Files']['#total'][GITHEALTH_MAJOR]++;
    $message = t('Bad line endings:') . $terminators;
    $reference = '318#indenting';
    $results['Files'][] = array(
      'type' => 'MAJOR',
      'message' => $message,
      'function' => $reference,
      'file' => $file,
    );
  }

  // Check for old CVS $Id$ tags
  $command = 'grep -rnI "\$Id" *';
  // We cannot use worker_execute here, as we expect a return status of 1,
  // which will pollute the log file.
  $result = exec($command, $output, $status);
  if ($status == 0) {
    foreach ($output as $line) {
      $params = explode(':', $line);
      $file = $params[0];
      $results['#total'][GITHEALTH_MAJOR]++;
      $results['Files']['#total'][GITHEALTH_MAJOR]++;
      $message = t('Remove all old CVS $Id tags, as they are no longer required. ');
      $reference = '';
      $results['Files'][] = array(
        'type' => 'MAJOR',
        'message' => $message,
        'function' => $reference,
        'file' => $file,
      );
    }
  }

  // Check for the byte order mark in text files
  foreach ($files['text'] as $file) {
    $command = "grep ^$'\\xEF\\xBB\\xBF' " . $file;
    // We cannot use worker_execute here, as we expect a return status of 1,
    // which will pollute the log file.
    $result = exec($command, $output, $status);
    if ($status == 0) {
      $results['#total'][GITHEALTH_MINOR]++;
      $results['Files']['#total'][GITHEALTH_MINOR]++;
      $message = t('Use of the byte order mark at the beginning of UTF-8 files is discouraged. Consider removing it.');
      $reference = '';
      $results['Files'][] = array(
        'type' => 'MINOR',
        'message' => $message,
        'function' => $reference,
        'file' => $file,
      );
    }
  }
}
